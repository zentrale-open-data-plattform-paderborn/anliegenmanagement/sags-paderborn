<?php

namespace Drupal\markaspot_rules\Plugin\BusinessRulesAction;

use Drupal\business_rules\ActionInterface;
use Drupal\business_rules\Events\BusinessRulesEvent;
use Drupal\business_rules\ItemInterface;
use Drupal\business_rules\Plugin\BusinessRulesActionPlugin;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AddNodeToOrganisation.
 *
 * @package Drupal\business_rules\Plugin\BusinessRulesAction
 *
 * @BusinessRulesAction(
 *   id = "add_node_to_organisation",
 *   label = @Translation("Add request to organisation"),
 *   group = @Translation("Entity"),
 *   description = @Translation("Add request to organisation (like Group-module)."),
 * )
 */
class AddNodeToOrganisation extends BusinessRulesActionPlugin {

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm(array &$form, FormStateInterface $form_state, ItemInterface $item) {

    $set = $item->getSettings('compare_tax_vocab_term');
    $compare_vocab_options = $this->util->getBundles('taxonomy_term');
    $form_state_vocab = $form_state->getValue('compare_tax_vocab');

    if (!empty($form_state->getValue('compare_tax_vocab_term'))) {
      $item->setSetting('compare_tax_vocab_term', $form_state->getValue('compare_tax_vocab_term'));
    }
    if (empty($form_state_vocab) && empty($item->getSettings('compare_tax_vocab'))) {
      // Use a default value.
      $selected_compare_vocab = key($compare_vocab_options);
    }
    else {
      $selected_compare_vocab = (!empty($form_state_vocab)) ? $form_state_vocab : $item->getSettings('compare_tax_vocab');
    }

    $settings['compare_tax_vocab'] = [
      '#type' => 'select',
      '#title' => t('Vocabulary to compare'),
      '#options' => $compare_vocab_options,
      '#default_value' => $selected_compare_vocab,
      '#ajax' => [
        'callback' => [get_class($this), 'dataComparisonCompareVocabCallback'],
        'wrapper' => 'compare-tax-term-container',
      ],
    ];

    $settings['compare_tax_vocab_term'] = [
      '#attributes' => ['id' => 'compare-tax-term-container'],
      '#type' => 'select',
      '#options' => self::getTermOptions($selected_compare_vocab),
      '#default_value' => !empty($form_state->getValue('compare_tax_vocab_term')) ? $form_state->getValue('compare_tax_vocab_term') : $set,
    ];

    if ($selected_compare_vocab == '') {
      $settings['compare_tax_vocab_term']['#title'] = t('You must choose a vocabulary first.');
      $settings['compare_tax_vocab_term']['#disabled'] = TRUE;
    }
    else {
      $settings['compare_tax_vocab_term']['#disabled'] = FALSE;
    }


    return $settings;
  }

  /**
   * Helper function to rebuild data comparison form after taxonomy vocab
   * selection.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AjaxResponse.
   */
  public static function dataComparisonCompareVocabCallback(array &$form, FormStateInterface $form_state) {

    return $form['settings']['compare_tax_vocab_term'];

  }

  /**
   * Get all user terms of taxonomy.
   *
   * @return array
   *   Options array.
   */
  public static function getTermOptions($vocabulary) {
    $terms = \Drupal::service('entity_type.manager')
      ->getStorage("taxonomy_term")
      ->loadMultiple();
    $options = [];

    /**@var  \Drupal\taxonomy\Entity\Term $term */
    foreach ($terms as $term) {
      $vocab = $term->getVocabularyId();
      if ($term->getVocabularyId() == $vocabulary) {
        $options[$term->id()] = $term->getName();
      }
    }
    asort($options);

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ActionInterface $action, BusinessRulesEvent $event) {

    $entity = $event->getArgument('entity');
    $group_id = $action->getSettings('compare_tax_vocab_term');

    $entity->field_organisation->target_id = $group_id;
    $entity->save();
  }

}
