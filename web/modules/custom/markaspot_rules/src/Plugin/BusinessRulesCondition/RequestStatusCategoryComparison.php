<?php

namespace Drupal\markaspot_rules\Plugin\BusinessRulesCondition;

use Drupal\business_rules\ConditionInterface;
use Drupal\business_rules\Events\BusinessRulesEvent;
use Drupal\business_rules\ItemInterface;
use Drupal\business_rules\Plugin\BusinessRulesConditionPlugin;
use Drupal\business_rules\VariablesSet;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DataComparison.
 *
 * @package Drupal\business_rules\BusinessRulesCondition
 *
 * @BusinessRulesCondition(
 *   id = "request_status_category_comparison",
 *   label = @Translation("Request Status and Category Comparison "),
 *   group = @Translation("Entity"),
 *   description = @Translation("Compare entity field value against entity reference."),
 *   isContextDependent = TRUE,
 *   reactsOnIds = {},
 *   hasTargetEntity = TRUE,
 *   hasTargetBundle = TRUE,
 *   hasTargetField = TRUE,
 *   useFlowChart = FALSE
 * )
 */
class RequestStatusCategoryComparison extends BusinessRulesConditionPlugin {

  /**
   * Helper function to rebuild data comparison form after taxonomy vocab
   * selection.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AjaxResponse.
   */
  public static function dataComparisonCompareVocabCallback(array &$form, FormStateInterface $form_state) {

    return $form['settings']['compare_tax_vocab_term'];

  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm(array &$form, FormStateInterface $form_state, ItemInterface $condition) {
    // Only show settings form if the item is already saved.
    if ($condition->isNew()) {
      return [];
    }
    $set = $condition->getSettings('compare_tax_vocab_term');

    $compare_vocab_options = $this->util->getBundles('taxonomy_term');

    $form_state_vocab = $form_state->getValue('compare_tax_vocab');

    if (!empty($form_state->getValue('compare_tax_vocab_term'))) {
      $condition->setSetting('compare_tax_vocab_term', $form_state->getValue('compare_tax_vocab_term'));
    }
    if (empty($form_state_vocab) && empty($condition->getSettings('compare_tax_vocab'))) {
      // Use a default value.
      $selected_compare_vocab = key($compare_vocab_options);
    }
    else {
      $selected_compare_vocab = (!empty($form_state_vocab)) ? $form_state_vocab : $condition->getSettings('compare_tax_vocab');
    }

    $settings['compare_tax_vocab'] = [
      '#type' => 'select',
      '#title' => t('Vocabulary to compare'),
      '#options' => $compare_vocab_options,
      '#default_value' => $selected_compare_vocab,
      '#ajax' => [
        'callback' => [get_class($this), 'dataComparisonCompareVocabCallback'],
        'wrapper' => 'compare-tax-term-container',
      ],
    ];

    $settings['compare_tax_vocab_term'] = [
      '#attributes' => ['id' => 'compare-tax-term-container'],

      '#type' => 'select',
      '#options' => self::getTermOptions($selected_compare_vocab),
      '#default_value' => !empty($form_state->getValue('compare_tax_vocab_term')) ? $form_state->getValue('compare_tax_vocab_term') : $set,
    ];
    /*
    $settings['original_current'] = [
      '#type' => 'checkbox',
      '#title' => t('Include original check'),
      '#default_value' => TRUE,
    ];
    */

    if ($selected_compare_vocab == '') {
      $settings['compare_tax_vocab_term']['#title'] = t('You must choose a vocabulary first.');
      $settings['compare_tax_vocab_term']['#disabled'] = TRUE;
    }
    else {
      $settings['compare_tax_vocab_term']['#disabled'] = FALSE;
    }


    return $settings;
  }


  /**
   * {@inheritdoc}
   */
  public function process(ConditionInterface $condition, BusinessRulesEvent $event) {
    /** @var \Drupal\Core\Entity\Entity $entity */
    $field = $condition->getSettings('field');
    $term_id = $condition->getSettings('compare_tax_vocab_term');

    $entity_current = $event->getArgument('entity');
    $entity_unchanged = $event->getArgument('entity_unchanged');
    $react_on = $event->getArgument('reacts_on');
    $current_values = $entity_current->get($field)->getValue();

    if ($react_on['id'] == 'entity_update') {
      $original_values = $entity_unchanged->get($field)->getValue();
      foreach ($original_values as $value) {
        if (isset($term_id) && isset($value)) {
          $entity_value = strip_tags(strtolower(trim($value['target_id'])));
          $compare_value = $term_id;

          $original_met = $this->util->criteriaMet($entity_value, '==', $compare_value);
        } else {
          $original_met = FALSE;
        }
      }
    } else {
      $original_met = FALSE;
    }
    if (!empty($current_values)) {
      foreach ($current_values as $value) {
        if (isset($term_id) && isset($value)) {
          $entity_value = strip_tags(strtolower(trim($value['target_id'])));
          $compare_value = $term_id;

          $current_met = $this->util->criteriaMet($entity_value, '==', $compare_value);
        }
      }
    } else {
      $current_met = FALSE;
    }
    if ($current_met == TRUE && $original_met == FALSE ) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get all user terms of taxonomy.
   *
   * @return array
   *   Options array.
   */
  public static function getTermOptions($vocabulary) {
    $terms = \Drupal::service('entity_type.manager')
      ->getStorage("taxonomy_term")
      ->loadMultiple();
    $options = [];

    /**@var  \Drupal\taxonomy\Entity\Term $term */
    foreach ($terms as $term) {
      $vocab = $term->getVocabularyId();
      if ($term->getVocabularyId() == $vocabulary) {
        $options[$term->id()] = $term->getName();
      }
    }
    asort($options);

    return $options;
  }

}