<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Drupal site-specific configuration file.
 */

// Config sync directory.
$settings['config_sync_directory'] = '../config/sync';

// Hash salt.
$settings['hash_salt'] = getenv('DRUPAL_HASH_SALT');

// Disallow access to update.php by anonymous users.
$settings['update_free_access'] = FALSE;

// Other helpful settings.
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
$settings['entity_update_batch_size'] = 50;
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

// Database connection.
$databases['default']['default'] = [
  'database' => getenv('DRUPAL_DATABASE_NAME'),
  'username' => getenv('DRUPAL_DATABASE_USERNAME'),
  'password' => getenv('DRUPAL_DATABASE_PASSWORD'),
  'prefix' => '',
  'host' => getenv('DRUPAL_DATABASE_HOST'),
  'port' => 3306,
  'namespace' => 'Drupal\Core\Database\Driver\mysql',
  'driver' => 'mysql',
];

// Add SMTP Setting by GITLAB ENV Specific secret variables.
$config['smtp.settings']['smtp_username'] = getenv('SMTP_USER');
$config['smtp.settings']['smtp_password'] = base64_decode(getenv('SMTP_PASSWORD'));

// Add API Key for API_USER as by GITLAB ENV Specific secret variable.
$config['services_api_key_auth.api_key.api_user_key']['key'] = getenv('GEOREPORT_API_USER_KEY');

// Verbose Log Level only in Dev-Environment.
if(getenv('USER') == "citizen-dev") {
  $config['system.logging']['error_level'] = 'verbose';
  $config['system.performance']['css']['preprocess'] = FALSE;
  $config['system.performance']['js']['preprocess'] = FALSE;
}


$settings['trusted_host_patterns'] = [ '.*' ];

// Local settings. These come last so that they can override anything.
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}

