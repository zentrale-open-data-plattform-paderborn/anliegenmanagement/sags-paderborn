# Mark-a-Spot for Paderborn

This is a composer-based installer for the Mark-a-Spot Drupal distribution. It can be used to start a project based on this crowdmapping platform.

Mark-a-Spot is a Public Civic Issue Tracking and Crowdsourcing / Mapping Platform, Open311 Server and Client Software.

This template builds Mark-a-Spot using the "Drupal Recommended" Composer project.
Drupal is a flexible and extensible PHP-based CMS framework.

## Install development:

```
 $ docker-compose up -d
 $ docker exec -it markaspot-for-kubernetes /bin/bash
```
or use the included Docksal integration.


## Post-install development

To install the latest development state please run within the container:

```
$ drush sql-drop -y
$ gunzip -c | drush sqlc < db/database.sql
$ drush cr
$ drush cim -y
```

## License
This work is licensed under the EUPL 1.2 or later. See [LICENSE](LICENSE) for additional information.

#### Author
Holger Kreis, Mark-a-Spot [Github](https://github.com/markaspot) | [Web](https://www.markaspot.de/en) | [Twitter](https://twitter.com/markaspot)
